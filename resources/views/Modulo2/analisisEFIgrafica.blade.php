@extends('layouts.nav2')

@section('content')
<header>
	@yield('js')
	@section('f')
	<a href="{{ route('home') }}" class="clos" aria-label="Close"><span class="icon-undo2"></span></a>
	@endsection
	@include('modal/modalGrafica')
</header>
<section id="containerGrafica2">
	<div id="TituloGrafica"><h2>Análisis EFE y EFI</h2></div>
<div class="btn-group-vertical">
  <button class="botonesGrafica" style="background: #0AB5A0; outline: none;" data-toggle="modal" data-target="#exampleModalCenter"></button>
  <button class="botonesGrafica" style="background: #FC7323; outline: none;" data-toggle="modal" data-target="#exampleModalCenter1"></button>
  <button class="botonesGrafica" style="background: #238276; outline: none;" data-toggle="modal" data-target="#exampleModalCenter2"></button>
</div>
	<div id="containerGrafica"></div>
</section>
<style type="text/css">
	.btn-group-vertical {
    position: absolute;
    margin: 180px 109px;
    z-index: 23;
	}
</style>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/highcharts-more.js"></script>

        <script>
// Build the chart
Highcharts.chart('containerGrafica', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: ''
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    accessibility: {
        point: {
            valueSuffix: '%'
        }
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: false
            },
            showInLegend: true
        }
    },
    series: [{
        name: 'Puntaje',
        colorByPoint: true,
        data: [{
            name: 'Retener y mantener.',
            y: 61.41,
            color: '#FC7323',
            sliced: true,
            selected: true
        }, {
            name: 'Crecer y construir.',
            color: '#0AB5A0',
            y: 41.67
        }, {
            name: 'Cosechar o desinvertir.',
            color: '#238276',
            y: 14.18
        }]
    }]
});
    </script>

	<button   type="submit" style="color:white;" name="nuevo" class="Ahora btn btn-planeem waves-effect waves-light">Iniciar Ahora</button>
<span class="icon-info" data-toggle="modal" data-target="#exampleModalScrollable" style="cursor:pointer;"></span>
<div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalCenterTitle" style="margin-left: 252px; font-weight: bold;"></h5>
				<span class="icon-cancel-circle" style="color:#FC7323; font-size: 32px; cursor: pointer; margin-top: 4px;
				margin-left: 10%;" data-dismiss="modal" aria-label="Close"></span>

			</div>
			<div class="modal-body">
				<p>
					Como realizar la calificación de la Matriz PCI (Perfil de Capacidad interna)

					Para realizar la calificación de la matriz se debe seleccionar la capacidad, identificar si
					es una fortaleza o debilidad para la empresa, luego si:
					<br><br>
					1. Es una fortaleza se debe calificar D si es débil (débil), M si es (media) y A si es (alta)
					<br>
					2. Es debilidad debo calificar si es D si es débil (débil), M si es (media) y A si es (alta)
					Luego, se califica que impacto tiene esa debilidad o fortaleza para la empresa: D(débil),
					M (media), A(alta)
				</p>
			</div>
		</div>
	</div>

</section>
@yield('script')


@endsection
