						<div class="modal fade" id="exampleModaleditar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content modal-modificado2">
									<div class="modal-body">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="outline: none;margin-left: 97% !important;">
											<span class="icon-cancel-circle" style="color: #FC7323; font-size: 21px;margin-top: 2%; cursor: pointer;"></span>
										</button>
										<div class="ingresevalor">
											<input type="text" class="campo2" name="nombreEmpresa" value="" placeholder="Ingresa tú nombre" style="background: none; outline:none;font-size: 20px;">
											
										</div>
										<br>
										<a  data-dismiss="modal" aria-label="Close" style="color:white;" class="aceptarvalor btn btn-planeem waves-effect waves-light">Aceptar</a>
									</div>
								</div>
							</div>
						</div>					
						{{-- modales de la vista Capacidad interna --}}
						<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="z-index: 9000;">
							<div class="modal-dialog" role="document">
								<div class="modal-content modal-modificado1">
									<div class="modal-body">
										<div id="cierre_caja4"><a data-dismiss="modal" aria-label="Close" style="background: white; outline: none !important; margin-left: 93%"><i class="icon-cancel-circle" style="color: #FC7323; font-size: 21px;margin-top: 2%; cursor: pointer;"></i></a>
											<p class="Nota">Nota: El perfil de fortalezas y debilidades, se representa gráficamente mediante la calificación de la fortaleza o debilidad con relación a su grado en la escala de Alto (A), Medio (M), y Bajo (B).</p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="z-index: 9000;">
							<div class="modal-dialog" role="document">
								<div class="modal-content modal-modificado1">
									<div class="modal-body">
										<div id="cierre_caja4"><a data-dismiss="modal" aria-label="Close" style="background: white; outline: none !important; margin-left: 93%"><i class="icon-cancel-circle" style="color: #FC7323; font-size: 21px;margin-top: 2%; cursor: pointer;"></i></a>
											<p class="Nota">Nota: El perfil de fortalezas y debilidades, se representa gráficamente mediante la calificación de la fortaleza o debilidad con relación a su grado en la escala de Alto (A), Medio (M), y Bajo (B).</p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="modal fade" id="exampleModal4" tabindex="-8" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="z-index: 9000;">
							<div class="modal-dialog" role="document">
								<div class="modal-content modal-modificado1">
									<div class="modal-body">
										<div id="cierre_caja7"><a data-dismiss="modal" aria-label="Close" style="background: white; outline: none !important; margin-left: 93%"><i class="icon-cancel-circle" style="color: #FC7323; font-size: 21px;margin-top: 2%; cursor: pointer;"></i></a>
											<p class="Nota">Nota: Son los factores internos que obstaculizan el logro de los objetivos planteados, incluyendo actividades y atributos internos de una organización que inhiben o dificultan el éxito de una empresa. (Prieto 2008)</p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="modal fade" id="exampleModal5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="z-index: 9000;">
							<div class="modal-dialog" role="document">
								<div class="modal-content modal-modificado1">
									<div class="modal-body">
										<div id="cierre_caja7"><a data-dismiss="modal" aria-label="Close" style="background: white; outline: none !important; margin-left: 93%"><i class="icon-cancel-circle" style="color: #FC7323; font-size: 21px;margin-top: 2%; cursor: pointer;"></i></a>
											<p class="Nota">Nota: Son los factores internos que obstaculizan el logro de los objetivos planteados, incluyendo actividades y atributos internos de una organización que inhiben o dificultan el éxito de una empresa. (Prieto 2008)</p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="modal fade" id="exampleModal6" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content modal-modificado1">
									<div class="modal-body">
										<div id="cierre_caja6"><a data-dismiss="modal" aria-label="Close" style="background: white; outline: none !important; margin-left: 93%"><i class="icon-cancel-circle" style="color: #FC7323; font-size: 21px;margin-top: 2%; cursor: pointer;"></i></a>
											<p class="Nota">Nota: Son todas las capacidades, atributos y recursos de una organización que contribuyen y apoyan el
											logro de los objetivos planificados con el fin de obtener ventajas competitivas. (Serna, 2010)</p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="modal fade" id="exampleModal7" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content modal-modificado1">
									<div class="modal-body">
										<div id="cierra_caja5"><a data-dismiss="modal" aria-label="Close" style="background: white; outline: none !important; margin-left: 93%"><i class="icon-cancel-circle" style="color: #FC7323; font-size: 21px;margin-top: 2%; cursor: pointer;"></i></a> 
											<p class="Nota">Nota: El perfil de fortalezas y debilidades, se representa gráficamente mediante la calificación de la fortaleza o debilidad con
											relación a su grado en la escala de Alto (A), Medio (M), y Bajo (B).</p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="modal fade" id="exampleModal8" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content modal-modificado1">
									<div class="modal-body">
										<div id="cierre_caja4"><a data-dismiss="modal" aria-label="Close" style="background: white; outline: none !important; margin-left: 93%"><i class="icon-cancel-circle" style="color: #FC7323; font-size: 21px;margin-top: 2%; cursor: pointer;"></i></a>
											<p class="Nota">Nota: El perfil de fortalezas y debilidades, se representa gráficamente mediante la calificación de la fortaleza o debilidad con relación a su grado en la escala de Alto (A), Medio (M), y Bajo (B).</p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="modal fade" id="exampleModal9" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content modal-modificado1">
									<div class="modal-body">
										<div id="cierre_caja7"><a data-dismiss="modal" aria-label="Close" style="background: white; outline: none !important; margin-left: 93%"><i class="icon-cancel-circle" style="color: #FC7323; font-size: 21px;margin-top: 2%; cursor: pointer;"></i></a>
											<p class="Nota">Nota: Son los factores internos que obstaculizan el logro de los objetivos planteados, incluyendo actividades y atributos internos de una organización que inhiben o dificultan el éxito de una empresa. (Prieto 2008)</p>
										</div>

									</div>
								</div>
							</div>
						</div>

						<div class="modal fade" id="exampleModal10" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content modal-modificado1">
									<div class="modal-body">
										<div id="cierre_caja6"><a data-dismiss="modal" aria-label="Close" style="background: white; outline: none !important; margin-left: 93%"><i class="icon-cancel-circle" style="color: #FC7323; font-size: 21px;margin-top: 2%; cursor: pointer;"></i></a>
											<p class="Nota">Nota: Son todas las capacidades, atributos y recursos de una organización que contribuyen y apoyan el
											logro de los objetivos planificados con el fin de obtener ventajas competitivas. (Serna, 2010)</p>
										</div>

									</div>
								</div>
							</div>
						</div>

						<div class="modal fade" id="exampleModal12" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content modal-modificado1">
									<div class="modal-body">
										<div id="cierre_caja4"><a data-dismiss="modal" aria-label="Close" style="background: white; outline: none !important; margin-left: 93%"><i class="icon-cancel-circle" style="color: #FC7323; font-size: 21px;margin-top: 2%; cursor: pointer;"></i></a>
											<p class="Nota">Nota: El perfil de fortalezas y debilidades, se representa gráficamente mediante la calificación de la fortaleza o debilidad con relación a su grado en la escala de Alto (A), Medio (M), y Bajo (B).</p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="modal fade" id="exampleModal13" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content modal-modificado1">
									<div class="modal-body">
										<div id="cierre_caja7"><a data-dismiss="modal" aria-label="Close" style="background: white; outline: none !important; margin-left: 93%"><i class="icon-cancel-circle" style="color: #FC7323; font-size: 21px;margin-top: 2%; cursor: pointer;"></i></a>
											<p class="Nota">Nota: Son los factores internos que obstaculizan el logro de los objetivos planteados, incluyendo actividades y atributos internos de una organización que inhiben o dificultan el éxito de una empresa. (Prieto 2008)</p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="modal fade" id="exampleModal11" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content modal-modificado1">
									<div class="modal-body">
										<div id="cierre_caja6"><a data-dismiss="modal" aria-label="Close" style="background: white; outline: none !important; margin-left: 93%"><i class="icon-cancel-circle" style="color: #FC7323; font-size: 21px;margin-top: 2%; cursor: pointer;"></i></a>
											<p class="Nota">Nota: Son todas las capacidades, atributos y recursos de una organización que contribuyen y apoyan el
											logro de los objetivos planificados con el fin de obtener ventajas competitivas. (Serna, 2010)</p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="modal fade" id="exampleModal14" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content modal-modificado1">
									<div class="modal-body">
										<div id="cierre_caja4"><a data-dismiss="modal" aria-label="Close" style="background: white; outline: none !important; margin-left: 93%"><i class="icon-cancel-circle" style="color: #FC7323; font-size: 21px;margin-top: 2%; cursor: pointer;"></i></a>
											<p class="Nota">Nota: El perfil de fortalezas y debilidades, se representa gráficamente mediante la calificación de la fortaleza o debilidad con relación a su grado en la escala de Alto (A), Medio (M), y Bajo (B).</p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="modal fade" id="exampleModal15" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content modal-modificado1">
									<div class="modal-body">
										<div id="cierre_caja7"><a data-dismiss="modal" aria-label="Close" style="background: white; outline: none !important; margin-left: 93%"><i class="icon-cancel-circle" style="color: #FC7323; font-size: 21px;margin-top: 2%; cursor: pointer;"></i></a>
											<p class="Nota">Nota: Son los factores internos que obstaculizan el logro de los objetivos planteados, incluyendo actividades y atributos internos de una organización que inhiben o dificultan el éxito de una empresa. (Prieto 2008)</p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="modal fade" id="exampleModal16" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content modal-modificado1">
									<div class="modal-body">
										<div id="cierre_caja6"><a data-dismiss="modal" aria-label="Close" style="background: white; outline: none !important; margin-left: 93%"><i class="icon-cancel-circle" style="color: #FC7323; font-size: 21px;margin-top: 2%; cursor: pointer;"></i></a>
											<p class="Nota">Nota: Son todas las capacidades, atributos y recursos de una organización que contribuyen y apoyan el
											logro de los objetivos planificados con el fin de obtener ventajas competitivas. (Serna, 2010)</p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="modal fade" id="exampleModal21" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content modal-modificado2">
									<div class="modal-body">
										<div class="añadircapacidad">
											<textarea maxlength="504"  id="Descripción" style="color:black;" class="campo4"></textarea>
										</div>
										<div><a style="color:white;" data-dismiss="modal" aria-label="Close" class="aceptarcapacidad btn btn-planeem waves-effect waves-light">Añadir</a> 
										</div>
										<div id="cancelar">
											<a value="cierra_AñadirCapa" class="cancelarcapacidad btn btn-planeem waves-effect waves-light" data-dismiss="modal" aria-label="Close" style=" outline: none !important;">Cancelar</a>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
							<div class="modal-dialog modal-dialog-scrollable" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalCenterTitle" style="margin-left: 252px; font-weight: bold;"></h5>
										<span class="icon-cancel-circle" style="color:#FC7323; font-size: 32px; cursor: pointer; margin-top: 4px;
										margin-left: 10%;" data-dismiss="modal" aria-label="Close"></span>

									</div>
									<div class="modal-body">
										<p>
											Como realizar la calificación de la Matriz PCI (Perfil de Capacidad interna)

											Para realizar la calificación de la matriz se debe seleccionar la capacidad, identificar si
											es una fortaleza o debilidad para la empresa, luego si:
											<br><br>
											1. Es una fortaleza se debe calificar D si es débil (débil), M si es (media) y A si es (alta)
											<br>
											2. Es debilidad debo calificar si es D si es débil (débil), M si es (media) y A si es (alta)
											Luego, se califica que impacto tiene esa debilidad o fortaleza para la empresa: D(débil),
											M (media), A(alta)
										</p>
									</div>
								</div>
							</div>
						</div>

						<div class="modal fade" id="exampleModal0" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content modal-modificado2">
									<div class="modal-body">
										<div class="añadircapacidad">
											<textarea maxlength="504"  id="Descripción" style="color:black;" class="campo4"></textarea>
										</div>
										<div><a style="color:white;" data-dismiss="modal" aria-label="Close" class="aceptarcapacidad btn btn-planeem waves-effect waves-light">Añadir</a> 
										</div>
										<div id="cancelar">
											<a value="cierra_AñadirCapa" class="cancelarcapacidad btn btn-planeem waves-effect waves-light" data-dismiss="modal" aria-label="Close" style=" outline: none !important;">Cancelar</a>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="modal fade" id="exampleModal001" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content modal-modificado1">
									<div class="modal-body">
										<div id="cierre_caja4"><a data-dismiss="modal" aria-label="Close" style="background: white; outline: none !important; margin-left: 93%"><i class="icon-cancel-circle" style="color: #FC7323; font-size: 21px;margin-top: 2%; cursor: pointer;"></i></a>
											<p class="Nota">Nota: El perfil de fortalezas y debilidades, se representa gráficamente mediante la calificación de la fortaleza o debilidad con
											relación a su grado en la escala de Alto (A), Medio (M), y Bajo (B).</p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="z-index: 9000;">
							<div class="modal-dialog" role="document">
								<div class="modal-content modal-modificado4">
									<div class="modal-body">
										<div id="cierre_proveedores"><a data-dismiss="modal" aria-label="Close" style="background: white; outline: none !important; margin-left: 97%"><i class="icon-cancel-circle" style="color: #FC7323; font-size: 21px;margin-top: 2%; cursor: pointer;"></i></a> 
											<p style="line-height: 17px; margin-top: 2px;">
												<b style="color: black; font-weight: bold; text-align: center;">Poder de negociación de los proveedores</b>
												<br><br>
												Un mercado o segmento del mercado, no será atractivo cuando los proveedores estén
												muy bien organizados gremialmente, tengan fuertes recursos y puedan imponer sus
												condiciones de precio y tamaño del pedido. La situación será aún más complicada si los
												insumos que suministran son claves, no tienen sustitutos o son pocos y de alto costo.
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="modal fade" id="exampleModal55" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content modal-modificado4">
									<div class="modal-body">
										<div id="cierre_proveedores"><a data-dismiss="modal" aria-label="Close" style="background: white; outline: none !important; margin-left: 97%"><i class="icon-cancel-circle" style="color: #FC7323; font-size: 21px;margin-top: 2%; cursor: pointer;"></i></a> 
											<p style="line-height: 17px; margin-top: 2px;">
												<b style="color: black; font-weight: bold; text-align: center;">Poder de negociación de lo clientes</b>
												<br><br>
												Un mercado o segmento no será atractivo cuando los clientes están muy bien
												organizados, el producto tiene varios o muchos sustitutos, el producto no es muy
												diferenciado o es de bajo costo para el cliente, lo que permite que pueda hacer
												sustituciones por igual o a muy bajo costo.
												A mayor organización de los compradores mayores serán sus exigencias en materia de
												reducción de precios, mayor calidad y servicios y por consiguiente la corporación tendrá
												una disminución en los márgenes de utilidad.
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="modal fade" id="exampleModal511" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content modal-modificado4">
									<div class="modal-body">
										<div id="cierre_proveedores"><a data-dismiss="modal" aria-label="Close" style="background: white; outline: none !important; margin-left: 97%"><i class="icon-cancel-circle" style="color: #FC7323; font-size: 21px;margin-top: 2%; cursor: pointer;"></i></a> 
											<p style="line-height: 17px; margin-top: 2px;">
												<b style="color: black; font-weight: bold; text-align: center;">Poder de negociación de lo clientes</b>
												<br><br>
												Un mercado o segmento no será atractivo cuando los clientes están muy bien
												organizados, el producto tiene varios o muchos sustitutos, el producto no es muy
												diferenciado o es de bajo costo para el cliente, lo que permite que pueda hacer
												sustituciones por igual o a muy bajo costo.
												A mayor organización de los compradores mayores serán sus exigencias en materia de
												reducción de precios, mayor calidad y servicios y por consiguiente la corporación tendrá
												una disminución en los márgenes de utilidad.
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="modal fade" id="exampleModal999" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content modal-modificado4">
									<div class="modal-body">
										<div id="cierre_proveedores"><a data-dismiss="modal" aria-label="Close" style="background: white; outline: none !important; margin-left: 97%"><i class="icon-cancel-circle" style="color: #FC7323; font-size: 21px;margin-top: 2%; cursor: pointer;"></i></a> 
											<p style="line-height: 17px; margin-top: 2px;">
												<b style="color: black; font-weight: bold; text-align: center;">Productos Sustitutos</b>
												<br><br>
												Un mercado o segmento no es atractivo si existen productos sustitutos reales o potenciales. La situación se complica si los sustitutos están más avanzados tecnológicamente o pueden entrar a precios más bajos reduciendo los márgenes de utilidad de la empresa y del sector. Para este tipo de modelo tradicional, la defensa consiste en construir barreras de entrada alrededor de una fortaleza que tuviera la empresa y que le permitiera, mediante la protección que le otorga esta ventaja competitiva, obtener utilidades que luego podía utilizar en investigación y desarrollo, para financiar una guerra de precios o para invertir en otros negocios.
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="modal fade" id="exampleModal134" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content modal-modificado4">
									<div class="modal-body">
										<div id="cierre_proveedores"><a data-dismiss="modal" aria-label="Close" style="background: white; outline: none !important; margin-left: 97%"><i class="icon-cancel-circle" style="color: #FC7323; font-size: 21px;margin-top: 2%; cursor: pointer;"></i></a> 
											<p style="line-height: 17px; margin-top: 2px;">
												<b style="color: black; font-weight: bold; text-align: center;">Productos Sustitutos</b>
												<br><br>
												Un mercado o segmento no es atractivo si existen productos sustitutos reales o potenciales. La situación se complica si los sustitutos están más avanzados tecnológicamente o pueden entrar a precios más bajos reduciendo los márgenes de utilidad de la empresa y del sector. Para este tipo de modelo tradicional, la defensa consiste en construir barreras de entrada alrededor de una fortaleza que tuviera la empresa y que le permitiera, mediante la protección que le otorga esta ventaja competitiva, obtener utilidades que luego podía utilizar en investigación y desarrollo, para financiar una guerra de precios o para invertir en otros negocios.
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="modal fade" id="exampleModal135" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content modal-modificado4">
									<div class="modal-body">
										<div id="cierre_proveedores"><a data-dismiss="modal" aria-label="Close" style="background: white; outline: none !important; margin-left: 97%"><i class="icon-cancel-circle" style="color: #FC7323; font-size: 21px;margin-top: 2%; cursor: pointer;"></i></a> 
											<p style="line-height: 17px; margin-top: 2px;">
												<b style="color: black; font-weight: bold; text-align: center;">Productos Sustitutos</b>
												<br><br>
												Un mercado o segmento no es atractivo si existen productos sustitutos reales o potenciales. La situación se complica si los sustitutos están más avanzados tecnológicamente o pueden entrar a precios más bajos reduciendo los márgenes de utilidad de la empresa y del sector. Para este tipo de modelo tradicional, la defensa consiste en construir barreras de entrada alrededor de una fortaleza que tuviera la empresa y que le permitiera, mediante la protección que le otorga esta ventaja competitiva, obtener utilidades que luego podía utilizar en investigación y desarrollo, para financiar una guerra de precios o para invertir en otros negocios.
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="modal fade" id="exampleModal99" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content modal-modificado4">
									<div class="modal-body">
										<div id="cierre_proveedores"><a data-dismiss="modal" aria-label="Close" style="background: white; outline: none !important; margin-left: 97%"><i class="icon-cancel-circle" style="color: #FC7323; font-size: 21px;margin-top: 2%; cursor: pointer;"></i></a> 
											<p style="line-height: 17px; margin-top: 2px;">
												<b style="color: black; font-weight: bold; text-align: center;">Productos Sustitutos</b>
												<br><br>
												Un mercado o segmento no es atractivo si existen productos sustitutos reales o potenciales. La situación se complica si los sustitutos están más avanzados tecnológicamente o pueden entrar a precios más bajos reduciendo los márgenes de utilidad de la empresa y del sector. Para este tipo de modelo tradicional, la defensa consiste en construir barreras de entrada alrededor de una fortaleza que tuviera la empresa y que le permitiera, mediante la protección que le otorga esta ventaja competitiva, obtener utilidades que luego podía utilizar en investigación y desarrollo, para financiar una guerra de precios o para invertir en otros negocios.
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="modal fade" id="exampleModal131" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content modal-modificado4">
									<div class="modal-body">
										<div id="cierre_proveedores"><a data-dismiss="modal" aria-label="Close" style="background: white; outline: none !important; margin-left: 97%"><i class="icon-cancel-circle" style="color: #FC7323; font-size: 21px;margin-top: 2%; cursor: pointer;"></i></a> 
											<p style="line-height: 17px; margin-top: 2px;">
												<b style="color: black; font-weight: bold; text-align: center;">Amenazas de entrada de nuevos competidores</b>
												<br><br>
												El mercado o el segmento no son atractivos dependiendo de si las barreras de entrada

												son fáciles o no de franquear por nuevos participantes que puedan llgaar con nuevos
												recursos y capacidades para apoderarse de una porción del mercado.
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="modal fade" id="exampleModal177" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content modal-modificado4">
									<div class="modal-body">
										<div id="cierre_proveedores"><a data-dismiss="modal" aria-label="Close" style="background: white; outline: none !important; margin-left: 97%"><i class="icon-cancel-circle" style="color: #FC7323; font-size: 21px;margin-top: 2%; cursor: pointer;"></i></a> 
											<p style="line-height: 17px; margin-top: 2px;">
												<b style="color: black; font-weight: bold; text-align: center;">Rivalidad entre competidores</b>
												<br><br>
												Para una organización será más difícil competir en un mercado o en uno de sus
												segmentos donde los competidores estén muy bien posicionados, sean muy numerosos y
												los costos fijos sean altos, pues constantemente estará enfrentada a guerras de precios,
												campañas publicitarias agresivas, promociones y entrada de nuevos productos.
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>