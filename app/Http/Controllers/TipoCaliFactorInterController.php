<?php

namespace App\Http\Controllers;

use App\Model\tipoCaliFactorInter;
use Illuminate\Http\Request;

class TipoCaliFactorInterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\tipoCaliFactorInter  $tipoCaliFactorInter
     * @return \Illuminate\Http\Response
     */
    public function show(tipoCaliFactorInter $tipoCaliFactorInter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\tipoCaliFactorInter  $tipoCaliFactorInter
     * @return \Illuminate\Http\Response
     */
    public function edit(tipoCaliFactorInter $tipoCaliFactorInter)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\tipoCaliFactorInter  $tipoCaliFactorInter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tipoCaliFactorInter $tipoCaliFactorInter)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\tipoCaliFactorInter  $tipoCaliFactorInter
     * @return \Illuminate\Http\Response
     */
    public function destroy(tipoCaliFactorInter $tipoCaliFactorInter)
    {
        //
    }
}
