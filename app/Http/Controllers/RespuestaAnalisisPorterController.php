<?php

namespace App\Http\Controllers;

use App\Model\respuestaAnalisisPorter;
use Illuminate\Http\Request;

class RespuestaAnalisisPorterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\respuestaAnalisisPorter  $respuestaAnalisisPorter
     * @return \Illuminate\Http\Response
     */
    public function show(respuestaAnalisisPorter $respuestaAnalisisPorter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\respuestaAnalisisPorter  $respuestaAnalisisPorter
     * @return \Illuminate\Http\Response
     */
    public function edit(respuestaAnalisisPorter $respuestaAnalisisPorter)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\respuestaAnalisisPorter  $respuestaAnalisisPorter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, respuestaAnalisisPorter $respuestaAnalisisPorter)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\respuestaAnalisisPorter  $respuestaAnalisisPorter
     * @return \Illuminate\Http\Response
     */
    public function destroy(respuestaAnalisisPorter $respuestaAnalisisPorter)
    {
        //
    }
}
