<?php

namespace App\Http\Controllers;

use App\Model\respuestaCapacidad;
use Illuminate\Http\Request;

class RespuestaCapacidadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\respuestaCapacidad  $respuestaCapacidad
     * @return \Illuminate\Http\Response
     */
    public function show(respuestaCapacidad $respuestaCapacidad)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\respuestaCapacidad  $respuestaCapacidad
     * @return \Illuminate\Http\Response
     */
    public function edit(respuestaCapacidad $respuestaCapacidad)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\respuestaCapacidad  $respuestaCapacidad
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, respuestaCapacidad $respuestaCapacidad)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\respuestaCapacidad  $respuestaCapacidad
     * @return \Illuminate\Http\Response
     */
    public function destroy(respuestaCapacidad $respuestaCapacidad)
    {
        //
    }
}
