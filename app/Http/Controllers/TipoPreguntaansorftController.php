<?php

namespace App\Http\Controllers;

use App\Model\TipoPreguntaansorft;
use Illuminate\Http\Request;

class TipoPreguntaansorftController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\TipoPreguntaansorft  $tipoPreguntaansorft
     * @return \Illuminate\Http\Response
     */
    public function show(TipoPreguntaansorft $tipoPreguntaansorft)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\TipoPreguntaansorft  $tipoPreguntaansorft
     * @return \Illuminate\Http\Response
     */
    public function edit(TipoPreguntaansorft $tipoPreguntaansorft)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\TipoPreguntaansorft  $tipoPreguntaansorft
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TipoPreguntaansorft $tipoPreguntaansorft)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\TipoPreguntaansorft  $tipoPreguntaansorft
     * @return \Illuminate\Http\Response
     */
    public function destroy(TipoPreguntaansorft $tipoPreguntaansorft)
    {
        //
    }
}
