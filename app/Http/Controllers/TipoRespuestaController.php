<?php

namespace App\Http\Controllers;

use App\Model\tipoRespuesta;
use Illuminate\Http\Request;

class TipoRespuestaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\tipoRespuesta  $tipoRespuesta
     * @return \Illuminate\Http\Response
     */
    public function show(tipoRespuesta $tipoRespuesta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\tipoRespuesta  $tipoRespuesta
     * @return \Illuminate\Http\Response
     */
    public function edit(tipoRespuesta $tipoRespuesta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\tipoRespuesta  $tipoRespuesta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tipoRespuesta $tipoRespuesta)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\tipoRespuesta  $tipoRespuesta
     * @return \Illuminate\Http\Response
     */
    public function destroy(tipoRespuesta $tipoRespuesta)
    {
        //
    }
}
