<?php

namespace App\Http\Controllers;

use App\Model\tipoAnalisisPorter;
use Illuminate\Http\Request;

class TipoAnalisisPorterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\tipoAnalisisPorter  $tipoAnalisisPorter
     * @return \Illuminate\Http\Response
     */
    public function show(tipoAnalisisPorter $tipoAnalisisPorter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\tipoAnalisisPorter  $tipoAnalisisPorter
     * @return \Illuminate\Http\Response
     */
    public function edit(tipoAnalisisPorter $tipoAnalisisPorter)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\tipoAnalisisPorter  $tipoAnalisisPorter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tipoAnalisisPorter $tipoAnalisisPorter)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\tipoAnalisisPorter  $tipoAnalisisPorter
     * @return \Illuminate\Http\Response
     */
    public function destroy(tipoAnalisisPorter $tipoAnalisisPorter)
    {
        //
    }
}
