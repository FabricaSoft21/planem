<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Model\PenetracionMercado;
class PenetracionMercadoController extends Controller
{


    public function storage(Request $request){

        
        $id_Planeacion = $request->input('id_Planeacion');
        $id_tipo_Penetración = $request->input('id_tipo_Penetración');
        $idTipo = $request->input('id_penetracion_mercadoanf');
        $pesoRelativo = $request->input('pesoRelativo');
        $Calificación = $request->input('Calificación');
        $pesoPonderado = $request->input('pesoPonderado');



        // dd($id_Planeacion,$id_tipo_Penetración,$idTipo,$pesoRelativo,$Calificación,$pesoPonderado);

            for ($a=0; $a < count($id_tipo_Penetración) ; $a++) { 
                for ($i = 0; $i < count($idTipo); $i++) {
                    
                    PenetracionMercado::updateorCreate(
                        [
                            'id_Planeacion' => $id_Planeacion,
                            'id_penetracion_mercadoanf' => $idTipo[$i],
                            'id_tipo_Penetracion' => $id_tipo_Penetración[$a],
                        ],
                        [
                        
                            'id_Planeacion' => $id_Planeacion,
                            'id_tipo_Penetracion' =>$id_tipo_Penetración[$a],
                            'id_penetracion_mercadoanf' => $idTipo[$i],
                            'Peso_Relativo' => $pesoRelativo[$i],
                            'Calificación' => $Calificación[$i],
                            'Peso_Ponderado' => $pesoPonderado[$i]
                        ]
                    );
                }    
            }
            
        $message = array(   
            'message' => 'Empresas Guardadas con Éxito',
            'alert-type' => 'success'
        );



        return view('Modulo2.factoresExternos')->with(compact('id_Planeacion'))->with($message);
    }
}
