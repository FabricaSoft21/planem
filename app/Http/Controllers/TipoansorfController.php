<?php

namespace App\Http\Controllers;

use App\Model\Tipoansorf;
use Illuminate\Http\Request;

class TipoansorfController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Tipoansorf  $tipoansorf
     * @return \Illuminate\Http\Response
     */
    public function show(Tipoansorf $tipoansorf)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Tipoansorf  $tipoansorf
     * @return \Illuminate\Http\Response
     */
    public function edit(Tipoansorf $tipoansorf)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Tipoansorf  $tipoansorf
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tipoansorf $tipoansorf)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Tipoansorf  $tipoansorf
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tipoansorf $tipoansorf)
    {
        //
    }
}
