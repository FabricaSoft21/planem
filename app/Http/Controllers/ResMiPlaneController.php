<?php

namespace App\Http\Controllers;

use App\Model\resMiPlane;
use Illuminate\Http\Request;

class ResMiPlaneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\resMiPlane  $resMiPlane
     * @return \Illuminate\Http\Response
     */
    public function show(resMiPlane $resMiPlane)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\resMiPlane  $resMiPlane
     * @return \Illuminate\Http\Response
     */
    public function edit(resMiPlane $resMiPlane)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\resMiPlane  $resMiPlane
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, resMiPlane $resMiPlane)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\resMiPlane  $resMiPlane
     * @return \Illuminate\Http\Response
     */
    public function destroy(resMiPlane $resMiPlane)
    {
        //
    }
}
