<?php

namespace App\Http\Controllers;

use App\Model\factorclave;
use Illuminate\Http\Request;

class FactorclaveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\factorclave  $factorclave
     * @return \Illuminate\Http\Response
     */
    public function show(factorclave $factorclave)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\factorclave  $factorclave
     * @return \Illuminate\Http\Response
     */
    public function edit(factorclave $factorclave)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\factorclave  $factorclave
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, factorclave $factorclave)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\factorclave  $factorclave
     * @return \Illuminate\Http\Response
     */
    public function destroy(factorclave $factorclave)
    {
        //
    }
}
