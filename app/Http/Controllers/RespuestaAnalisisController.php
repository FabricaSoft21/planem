<?php

namespace App\Http\Controllers;

use App\Model\respuestaAnalisis;
use Illuminate\Http\Request;

class RespuestaAnalisisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\respuestaAnalisis  $respuestaAnalisis
     * @return \Illuminate\Http\Response
     */
    public function show(respuestaAnalisis $respuestaAnalisis)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\respuestaAnalisis  $respuestaAnalisis
     * @return \Illuminate\Http\Response
     */
    public function edit(respuestaAnalisis $respuestaAnalisis)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\respuestaAnalisis  $respuestaAnalisis
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, respuestaAnalisis $respuestaAnalisis)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\respuestaAnalisis  $respuestaAnalisis
     * @return \Illuminate\Http\Response
     */
    public function destroy(respuestaAnalisis $respuestaAnalisis)
    {
        //
    }
}
