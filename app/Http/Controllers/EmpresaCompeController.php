<?php

namespace App\Http\Controllers;

use App\Model\empresaCompe;
use Illuminate\Http\Request;

class EmpresaCompeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\empresaCompe  $empresaCompe
     * @return \Illuminate\Http\Response
     */
    public function show(empresaCompe $empresaCompe)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\empresaCompe  $empresaCompe
     * @return \Illuminate\Http\Response
     */
    public function edit(empresaCompe $empresaCompe)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\empresaCompe  $empresaCompe
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, empresaCompe $empresaCompe)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\empresaCompe  $empresaCompe
     * @return \Illuminate\Http\Response
     */
    public function destroy(empresaCompe $empresaCompe)
    {
        //
    }
}
