<?php

namespace App\Http\Controllers;

use App\Model\respuestaPerfilCompe;
use Illuminate\Http\Request;

class RespuestaPerfilCompeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\respuestaPerfilCompe  $respuestaPerfilCompe
     * @return \Illuminate\Http\Response
     */
    public function show(respuestaPerfilCompe $respuestaPerfilCompe)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\respuestaPerfilCompe  $respuestaPerfilCompe
     * @return \Illuminate\Http\Response
     */
    public function edit(respuestaPerfilCompe $respuestaPerfilCompe)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\respuestaPerfilCompe  $respuestaPerfilCompe
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, respuestaPerfilCompe $respuestaPerfilCompe)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\respuestaPerfilCompe  $respuestaPerfilCompe
     * @return \Illuminate\Http\Response
     */
    public function destroy(respuestaPerfilCompe $respuestaPerfilCompe)
    {
        //
    }
}
