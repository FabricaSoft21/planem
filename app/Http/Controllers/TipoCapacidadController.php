<?php

namespace App\Http\Controllers;

use App\Model\tipoCapacidad;
use Illuminate\Http\Request;

class TipoCapacidadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\tipoCapacidad  $tipoCapacidad
     * @return \Illuminate\Http\Response
     */
    public function show(tipoCapacidad $tipoCapacidad)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\tipoCapacidad  $tipoCapacidad
     * @return \Illuminate\Http\Response
     */
    public function edit(tipoCapacidad $tipoCapacidad)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\tipoCapacidad  $tipoCapacidad
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tipoCapacidad $tipoCapacidad)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\tipoCapacidad  $tipoCapacidad
     * @return \Illuminate\Http\Response
     */
    public function destroy(tipoCapacidad $tipoCapacidad)
    {
        //
    }
}
