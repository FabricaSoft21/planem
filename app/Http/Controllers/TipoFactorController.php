<?php

namespace App\Http\Controllers;

use App\Model\TipoFactor;
use Illuminate\Http\Request;

class TipoFactorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\TipoFactor  $tipoFactor
     * @return \Illuminate\Http\Response
     */
    public function show(TipoFactor $tipoFactor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\TipoFactor  $tipoFactor
     * @return \Illuminate\Http\Response
     */
    public function edit(TipoFactor $tipoFactor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\TipoFactor  $tipoFactor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TipoFactor $tipoFactor)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\TipoFactor  $tipoFactor
     * @return \Illuminate\Http\Response
     */
    public function destroy(TipoFactor $tipoFactor)
    {
        //
    }
}
