<?php

namespace App\Http\Controllers;

use App\Model\tipoAnalisis;
use Illuminate\Http\Request;

class TipoAnalisisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\tipoAnalisis  $tipoAnalisis
     * @return \Illuminate\Http\Response
     */
    public function show(tipoAnalisis $tipoAnalisis)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\tipoAnalisis  $tipoAnalisis
     * @return \Illuminate\Http\Response
     */
    public function edit(tipoAnalisis $tipoAnalisis)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\tipoAnalisis  $tipoAnalisis
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, tipoAnalisis $tipoAnalisis)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\tipoAnalisis  $tipoAnalisis
     * @return \Illuminate\Http\Response
     */
    public function destroy(tipoAnalisis $tipoAnalisis)
    {
        //
    }
}
