<?php

use Illuminate\Database\Seeder;
use App\Model\capacidad;

class capacidadTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //directiva
        capacidad::create([
            'nombre'=>'La imagen corporativa de su empresa.',
            'idTipo'=> 1

        ]);

        capacidad::create([
            'nombre'=>'El análisis estratégico.',
            'idTipo'=> 1

        ]);
        capacidad::create([
            'nombre'=>'El uso de planes estratégicos.',
            'idTipo'=> 1

        ]);
        capacidad::create([
            'nombre'=>'La evaluación y pronostico del medio.',
            'idTipo'=> 1

        ]);
        capacidad::create([
            'nombre'=>'Relaciona los componentes de la empresa.',
            'idTipo'=> 1
            
        ]);
        capacidad::create([
            'nombre'=>'La conceptualización de los emprendimientos de la empresa como una oportunidad.',
            'idTipo'=> 1
            
        ]);

        capacidad::create([
            'nombre'=>'La velocidad de respuesta a condiciones cambiantes.',
            'idTipo'=> 1

        ]);

        capacidad::create([
            'nombre'=>'La flexibilidad de la estructura organizacional.',
            'idTipo'=> 1

        ]);

        capacidad::create([
            'nombre'=>'La limitada capacidad productiva.',
            'idTipo'=> 1

        ]);

        capacidad::create([
            'nombre'=>'La capacidad del servicio y lealtad al cliente.',
            'idTipo'=> 1

        ]);

        capacidad::create([
            'nombre'=>'La orientación empresarial.',
            'idTipo'=> 1

        ]);

        capacidad::create([
            'nombre'=>'La habilidad para manejar la inflación.',
            'idTipo'=> 1

        ]);
        capacidad::create([
            'nombre'=>'La disponibilidad de líneas de crédito.',
            'idTipo'=> 1

        ]);
        capacidad::create([
            'nombre'=>'La capacidad de endeudamiento.',
            'idTipo'=> 1

        ]);
        capacidad::create([
            'nombre'=>'La exclusividad.',
            'idTipo'=> 1

        ]);

        capacidad::create([
            'nombre'=>'La rotación de inventarios.',
            'idTipo'=> 1

        ]);

        capacidad::create([
            'nombre'=>'El cumplimento de metas.',
            'idTipo'=> 1

        ]);

        capacidad::create([
            'nombre'=>'La planificación operativa.',
            'idTipo'=> 1

        ]);

        capacidad::create([
            'nombre'=>'La capacidad de negociación.',
            'idTipo'=> 1

        ]);

        capacidad::create([
            'nombre'=>'El sistema de coordinación.',
            'idTipo'=> 1

        ]);

        capacidad::create([
            'nombre'=>'El sistema de toma de decisiones.',
            'idTipo'=> 1

        ]);
        //Competitiva
        capacidad::create([
            'nombre'=>'La relación de los componentes de la empresa.',
            'idTipo'=> 2

        ]);
        capacidad::create([
            'nombre'=>'El Programa de posventa de su empresa.',
            'idTipo'=> 2

        ]);
        capacidad::create([
            'nombre'=>'La calidad en el producto.',
            'idTipo'=> 2

        ]);
        capacidad::create([
            'nombre'=>'La experiencia en el sector.',
            'idTipo'=> 2

        ]);
        capacidad::create([
            'nombre'=>'El entorno laboral inestable.',
            'idTipo'=> 2

        ]);
        capacidad::create([
            'nombre'=>'Lealtad y satisfacción del cliente.',
            'idTipo'=> 2

        ]);
        capacidad::create([
            'nombre'=>'La participación de su empresa en el mercado.',
            'idTipo'=> 2

        ]);
        capacidad::create([
            'nombre'=>'El bajo costo de distribución y ventas.',
            'idTipo'=> 2

        ]);
        capacidad::create([
            'nombre'=>'El portafolio de productos.',
            'idTipo'=> 2

        ]);
        capacidad::create([
            'nombre'=>'El uso del ciclo de vida del producto y reposición.',
            'idTipo'=> 2

        ]);

        capacidad::create([
            'nombre'=>'La Inversión en I+D+i para el desarrollo de nuevos productos.',
            'idTipo'=> 2

        ]);
        capacidad::create([
            'nombre'=>'La ventaja del potencial de crecimiento del mercado. ',
            'idTipo'=> 2

        ]);
        capacidad::create([
            'nombre'=>'La fortaleza de proveedores y disponibilidad de insumos.',
            'idTipo'=> 2

        ]);
        capacidad::create([
            'nombre'=>'La concentración de consumidores.',
            'idTipo'=> 2

        ]);
        capacidad::create([
            'nombre'=>'La administración de sus clientes.',
            'idTipo'=> 2

        ]);
        capacidad::create([
            'nombre'=>'El servicio Postventa.',
            'idTipo'=> 2

        ]);
        capacidad::create([
            'nombre'=>'La competencia en el sector.',
            'idTipo'=> 2

        ]);
        capacidad::create([
            'nombre'=>'El acceso a organismos privados y públicos.',
            'idTipo'=> 2

        ]);
        capacidad::create([
            'nombre'=>'La imagen de la compañía en el mercado.',
            'idTipo'=> 2

        ]);

        //Financiera
        capacidad::create([
            'nombre'=>'La comunicación y el control gerencial.',
            'idTipo'=> 3

        ]);
        capacidad::create([
            'nombre'=>'El acceso a capital cuando lo requiere.',
            'idTipo'=> 3

        ]);
        capacidad::create([
            'nombre'=>'La rentabilidad, del retorno de la inversión.',
            'idTipo'=> 3

        ]);
        capacidad::create([
            'nombre'=>'El grado de utilización de capacidad de endeudamiento.',
            'idTipo'=> 3

        ]);
        capacidad::create([
            'nombre'=>'La liquidez, disponibilidad de fondos internos.',
            'idTipo'=> 3

        ]);
        capacidad::create([
            'nombre'=>'Habilidad para competir con precios.',
            'idTipo'=> 3

        ]);
        capacidad::create([
            'nombre'=>'La comunicación y el control gerencial.',
            'idTipo'=> 3

        ]);
        capacidad::create([
            'nombre'=>'La estabilidad de costos.',
            'idTipo'=> 3

        ]);
        capacidad::create([
            'nombre'=>'La Inversión de capital.',
            'idTipo'=> 3

        ]);
        capacidad::create([
            'nombre'=>'La capacidad para satisfacer la demanda.',
            'idTipo'=> 3

        ]);

        capacidad::create([
            'nombre'=>'La habilidad para mantener el esfuerzo ante la demanda cíclica.',
            'idTipo'=> 3

        ]);

        capacidad::create([
            'nombre'=>'La elasticidad de la demanda con respecto a los precios.',
            'idTipo'=> 3

        ]);
        capacidad::create([
            'nombre'=>'La fuente de financiamiento interno y externo.',
            'idTipo'=> 3

        ]);
        capacidad::create([
            'nombre'=>'La disponibilidad de líneas de crédito.',
            'idTipo'=> 3

        ]);
        capacidad::create([
            'nombre'=>'La estabilidad de costos.',
            'idTipo'=> 3

        ]);
        capacidad::create([
            'nombre'=>'El tiempo que su negocio toma en cobrar la cartera.',
            'idTipo'=> 3

        ]);
        capacidad::create([
            'nombre'=>'La rotación de Inventario.',
            'idTipo'=> 3

        ]);
        capacidad::create([
            'nombre'=>'La poca demanda de su producto o servicio.',
            'idTipo'=> 3

        ]);
        capacidad::create([
            'nombre'=>'La oferta en su producto o servicio.',
            'idTipo'=> 3

        ]);
        //Tecnologica

        capacidad::create([
            'nombre'=>'La habilidad técnica y de manufactura.',
            'idTipo'=> 4

        ]);
        capacidad::create([
            'nombre'=>'La capacidad de innovación.',
            'idTipo'=> 4

        ]);
        capacidad::create([
            'nombre'=>'La rentabilidad.',
            'idTipo'=> 4

        ]);

        capacidad::create([
            'nombre'=>'El retorno de la inversión.',
            'idTipo'=> 4

        ]);
        capacidad::create([
            'nombre'=>'El nivel de tecnología utilizada en productos.',
            'idTipo'=> 4

        ]);
        capacidad::create([
            'nombre'=>'La fuerza de Patentes y procesos.',
            'idTipo'=> 4

        ]);
        capacidad::create([
            'nombre'=>'La efectividad de la producción y programas de entrega.',
            'idTipo'=> 4

        ]);

        capacidad::create([
            'nombre'=>'El valor agregado al producto.',
            'idTipo'=> 4

        ]);
        capacidad::create([
            'nombre'=>'La intensidad de mano de obra en el producto.',
            'idTipo'=> 4

        ]);
        capacidad::create([
            'nombre'=>'La aplicación de tecnologías informáticas.',
            'idTipo'=> 4

        ]);
        capacidad::create([
            'nombre'=>'La fuerza de producto.',
            'idTipo'=> 4

        ]);

        capacidad::create([
            'nombre'=>'La calidad y exclusividad de su producto o servicio.',
            'idTipo'=> 4

        ]);
        capacidad::create([
            'nombre'=>'La participación del mercado.',
            'idTipo'=> 4

        ]);
        capacidad::create([
            'nombre'=>'El bajo costo de distribución y ventas.',
            'idTipo'=> 4

        ]);
        capacidad::create([
            'nombre'=>'El portafolio de productos.',
            'idTipo'=> 4

        ]);
        capacidad::create([
            'nombre'=>'El nivel de desarrollo tecnológico.',
            'idTipo'=> 4

        ]);
        capacidad::create([
            'nombre'=>'El grado de implantación de tecnologías de la información.',
            'idTipo'=> 4

        ]);
        capacidad::create([
            'nombre'=>'El poder de negociación con los clientes.',
            'idTipo'=> 4

        ]);
        capacidad::create([
            'nombre'=>'La rivalidad entre competidores.',
            'idTipo'=> 4

        ]);
        capacidad::create([
            'nombre'=>'La ubicación geográfica.',
            'idTipo'=> 4

        ]);
        //factor humano

        capacidad::create([
            'nombre'=>'La aplicación de tecnologías informáticas.',
            'idTipo'=> 5

        ]);

        capacidad::create([
            'nombre'=>'La habilidad técnica y de manufactura.',
            'idTipo'=> 5

        ]);

        capacidad::create([
            'nombre'=>'La capacidad de innovación.',
            'idTipo'=> 5

        ]);

        capacidad::create([
            'nombre'=>'El nivel de tecnología utilizada en productos.',
            'idTipo'=> 5

        ]);

        capacidad::create([
            'nombre'=>'La fuerza de patentes y procesos.',
            'idTipo'=> 5

        ]);

        capacidad::create([
            'nombre'=>'La efectividad de la producción y programas de entrega.',
            'idTipo'=> 5

        ]);

        capacidad::create([
            'nombre'=>'El valor agregado al producto.',
            'idTipo'=> 5

        ]);

        capacidad::create([
            'nombre'=>'El nivel tecnológico.',
            'idTipo'=> 5

        ]);

        capacidad::create([
            'nombre'=>'La intensidad de mano de obra en el producto.',
            'idTipo'=> 5

        ]);
        capacidad::create([
            'nombre'=>'La aplicación de tecnologías informáticas.',
            'idTipo'=> 5

        ]);
        capacidad::create([
            'nombre'=>'La estabilidad.',
            'idTipo'=> 5

        ]);
        capacidad::create([
            'nombre'=>'La experiencia técnica.',
            'idTipo'=> 5

        ]);
        capacidad::create([
            'nombre'=>'El nivel académico del recurso humano.',
            'idTipo'=> 5

        ]);
        capacidad::create([
            'nombre'=>'La rotación interna.',
            'idTipo'=> 5

        ]);
        
        capacidad::create([
            'nombre'=>'El ausentismo.',
            'idTipo'=> 5

        ]);
        capacidad::create([
            'nombre'=>'La falta de capacitación.',
            'idTipo'=> 5

        ]);
        capacidad::create([
            'nombre'=>'El salario bajo.',
            'idTipo'=> 5

        ]);
        capacidad::create([
            'nombre'=>'El capital de trabajo mal utilizado.',
            'idTipo'=> 5

        ]);
        capacidad::create([
            'nombre'=>'Los recursos financieros.',
            'idTipo'=> 5

        ]);
        capacidad::create([
            'nombre'=>'La calidad en el producto final.',
            'idTipo'=> 5

        ]);
        capacidad::create([
            'nombre'=>'La buena actitud del personal contratado.',
            'idTipo'=> 5

        ]);

    }
}
